import React from 'react';
import './OneJoke.css';
const OneJoke=props=>{
    return (
        <div className="joke">
            <p>{props.value}</p>
        </div>
    );
};
export default OneJoke;