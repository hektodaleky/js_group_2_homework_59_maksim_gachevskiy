import React, {Component, Fragment} from "react";
import OneJoke from "../components/OneJoke";
import "./JokeBox.css";

class JokeBox extends Component {
    state = {
        posts: [],
        countValue: 10


    };
    changeCount = event => {
        this.setState({countValue: +event.target.value});
    };
    resetPosts = () => () => this.setState({posts: []});


    componentDidMount() {
        for (let i = 0; i < this.state.countValue; i++) {
            fetch('https://api.chucknorris.io/jokes/random').then(response => {
                if (response.ok) {
                    return response.json();
                }
                throw new Error('Something went wrong with network request');

            }).then(posts => {


                let newPosts = [...this.state.posts];
                newPosts.push(posts);
                this.setState({posts: newPosts});
            }).catch(error => {
                console.log(error);
            })
        }

    };


    render() {
        console.log(this.state);

        return (
            <Fragment>


                <div className="box">

                    {this.state.posts.map(post => {
                        return <OneJoke value={post.value} key={post.id}/>
                    })
                    }</div>
            </Fragment>)
    }
}

export default JokeBox;