import React, {Component, Fragment} from "react";
import InputCinema from "../../components/InputCinema/InputCinema";
import OneFieldCinema from "../../components/OneFieldCInema/OneFieldCinema";
import './Cinema.css';
class Cinema extends Component {
    state = {
        movies: [],
        inputState: ""
    };
    changeInputInfo = event => this.setState({inputState: event.target.value});
    saveOneMovie = () => {
        const oneRecord = {rec: this.state.inputState, id: Date.now()};
        let movies = [...this.state.movies];
        movies.push(oneRecord);
        this.setState({movies});
    };

    editMovie = (event, id) => {
        const index = this.state.movies.findIndex(find => find.id === id);
        let tmpList = [...this.state.movies];
        tmpList[index].rec = event.target.value;
        this.setState({movies: tmpList});
    };
    removeMovie = (id) => {
        const index = this.state.movies.findIndex(find => find.id === id);
        let tmpList = [...this.state.movies];

        tmpList = tmpList.filter(item => item != tmpList[index]);
        this.setState({movies: tmpList});

    };

    render() {
        return (
            <div className="Cinema">
                <InputCinema change={this.changeInputInfo} click={this.saveOneMovie}/>
                <p>To watch list</p>
                {this.state.movies.map(element => {
                    return <OneFieldCinema value={element.rec} key={element.id} id={element.id} changer={(event) => {
                        this.editMovie(event, element.id)
                    }} remove={() => {
                        this.removeMovie(element.id)
                    }}/>
                })}
            </div>
        );
    }
}
;
export default Cinema;