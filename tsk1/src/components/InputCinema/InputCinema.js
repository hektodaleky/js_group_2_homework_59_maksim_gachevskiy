import React from 'react';
import './InputCinema.css';
const InputCinema=props=>{
    return (  <div className="inputMenu">
        <input onChange={props.change} className="newCinema"/>
        <button onClick={props.click}>Add</button>
    </div>)
};
export default InputCinema;
