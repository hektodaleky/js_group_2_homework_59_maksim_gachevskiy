import React, {Component, Fragment} from "react";
import "./OneFieldCinema.css";
class OneFieldCinema extends Component {
    constructor(props) {
        super(props);
    }

    shouldComponentUpdate(nextProps, nextState) {
        return (nextProps.value !== this.props.value && nextProps.id === this.props.id);
    };

    render() {
        return (
            <div className='oneRecord'>
                <input value={this.props.value} onChange={this.props.changer}/>
                <p onClick={this.props.remove}>X</p>
            </div>)
    }

}

export default OneFieldCinema;